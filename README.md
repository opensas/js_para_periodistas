# JS para periodistas

## o Cómo aprendí a perderle el miedo a JavaScript

Recursos para [taller introductorio de programación web](https://mediaparty2018.sched.com/event/FzbK) para periodistas para la [Media Party 2018](http://mediaparty.info/en/).

## Descripción del Workshop

Ver la [presentación](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/) del workshop

_Aprender rompiendo: Introducción a JavaScript para Periodistas con un enfoque práctico_

Los participantes aprenderán los rudimentos básicos de JavaScript como para poder utilizar y adaptar librerías populares de JS (Como C3.js, LeaftLet, TimelineJS y otras) sin morir en el intento.

A lo largo del workshop los participantes pasarán por la experiencia de enfrentarse a nuevas tecnologías, leer la documentación, buscar ayuda, buscar en google, leer, entender y copiar ejemplos de código, visitar Stack Overflow y aprender a través de pruebas y errores, tal como hacen todos los días los desarolladores de software.

## Corriendo los ejemplos

Clonar el proyecto con

```bash
git clone https://gitlab.com/opensas/js_para_periodistas.git
```

O descargar el archivo zip desde [aquí](https://gitlab.com/opensas/js_para_periodistas/-/archive/master/js_para_periodistas-master.zip) y descomprimirlo

Luego

```
cd js_para_periodistas
```

Iniciá un [servidor local](local_server.md) y visitá el sitio. [Acá](local_server.md) tenés instrucciones para instalar y configurar varios servidores locales.

Allí vas a encontrar links para ejecutar los siguientes ejemplos.

Todos los ejemplos se encuentran en la carpeta [src](src/)

E iniciar un servidor http local:

## Contenido del Workshop

1. [Qué es JavaScript](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/3)

1. [Tipos de datos simples](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/16)

1. [Variables](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/23)

1. [Tipos de datos complejos: Arrays, Objectos y Funciones](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/25)

1. [JSON: JavaScript Object Notation](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/30)

1. [Bucles y ejecución condicional](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/34)

1. [Arreglamos un bug](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/37)

1. [Ejemplo con Leaflet.js](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/38)

1. [Ejemplo con C3.js](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/47)

1. [Mapa de Cultura: Un ejemplo completo con JavaScript, Leaflet y la API de Carto](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/52)

1. [Agradecimientos](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/55)

## Recursos

Repositorio en [GitLab](https://gitlab.com/): [https://gitlab.com/opensas/js_para_periodistas](https://gitlab.com/opensas/js_para_periodistas)

Diapositivas: [link](https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/)

## Thimbles de ejemplo

A continuación los ejemplos corriendo en [Thimble](https://thimble.mozilla.org). Sólo tienen que apretar el botón _Mezclar_/_Remix_ para crear una copia y comenzar a experimentar con el código.

### Qué es JavaScript

01. [Qué es JavaScript - html](src/que_es_js_01) | Ver en [thimble](https://thimbleprojects.org/opensas/533202)

02. [Qué es JavaScript - css](src/que_es_js_02) | Ver en [thimble](https://thimbleprojects.org/opensas/533205)

03. [Qué es JavaScript - js](src/que_es_js_03) | Ver en [thimble](https://thimbleprojects.org/opensas/533207)

04. [Qué es JavaScript - archivos separados](src/que_es_js_04) | Ver en [thimble](https://thimbleprojects.org/opensas/533212)

### Jugando con los resultados del mundial

01. [Resultados del Mundial](src/resultados_mundial) | Ver en [thimble](https://thimbleprojects.org/opensas/533507)

02. [Resultados del Mundial - solución](src/resultados_mundial_solucion) | Ver en [thimble](https://thimbleprojects.org/opensas/533622)

### Perfil de usuario - un ejemplo con Bootstrap y Vue.js

01. [Perfil del usuario](src/perfil_de_usuario) | Ver en [thimble](https://thimbleprojects.org/opensas/533499)

### Solucionando un bug

01. [Qué es JavaScript - bug](src/que_es_js_04) | Ver en [thimble](https://thimbleprojects.org/opensas/533207)

### Ejemplo con LeafLet.js

01. [Leaflet - ejemplo básico](src/ejemplo_leaflet_01) | Ver en [thimble](https://thimbleprojects.org/opensas/533714)

02. [Leaflet - agregamos puntos](src/ejemplo_leaflet_02) | Ver en [thimble](https://thimbleprojects.org/opensas/533714)

03. [Leaflet - trabajando con arrays](src/ejemplo_leaflet_03) | Ver en [thimble](https://thimbleprojects.org/opensas/533717)

04. [Leaflet - creamos nuestra librería](src/ejemplo_leaflet_04) | Ver en [thimble](https://thimbleprojects.org/opensas/533721)

05. [Leaflet - backend con google docs](src/ejemplo_leaflet_05) | Ver en [thimble](https://thimbleprojects.org/opensas/533846)

06. [Leaflet - backend con AirTable](src/ejemplo_leaflet_06) | Ver en [thimble](https://thimbleprojects.org/opensas/533921)

### Ejemplo con C3.js

01. [C3 - ejemplo básico](src/c3_ejemplo_01) | Ver en [thimble](https://thimbleprojects.org/opensas/534012)

02. [C3 - investigamos opciones de configuración](src/c3_ejemplo_02) | Ver en [thimble](https://thimbleprojects.org/opensas/533110)

03. [C3 - creamos nuestra librería](src/c3_ejemplo_03) | Ver en [thimble](https://thimbleprojects.org/opensas/534027)

05. [C3 - backend con google docs](src/c3_ejemplo_04)

06. [C3 - backend con AirTable](src/c3_ejemplo_05) | Ver en [thimble](https://thimbleprojects.org/opensas/534086/)

### Un ejemplo completo con JavaScript, Leaflet y la API de Carto

01. [Mapa de Cultura](src/mapa_cultura) | Ver en [thimble](https://thimbleprojects.org/opensas/534091)

La aplicación corriendo en [www.nardoz.com/mapa-cultura](http://www.nardoz.com/mapa-cultura)

Diapositivas de la [presentación](http://www.nardoz.com/mapa-cultura/slides/mediaparty_rendered.svg)

[Repositorio de GitHub](https://github.com/Nardoz/mapa-cultura)