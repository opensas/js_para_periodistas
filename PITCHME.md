---?image=assets/terror/images/psycho_opaque.jpg

### Como aprendí a perderle el miedo a...

# @fa[skull gp-tip]JavaScript

- Introducción a JavaScript para Periodistas con un enfoque práctico... |

---

### ¿Qué es lo que nos vamos a llevar de este taller?

- Cómo hacer una aplicación mobile desde cero |
- Convertirnos en unos JavaScript Ninja |
- Que nuestro linkedin reviente de ofertas en dólares |
- La paz en el mundo |
- ...seamos realistas |

---

### ¿Qué es lo que realmente nos vamos a llevar de este taller?

- Introducción básica a JavaScript |
- Comprender cómo se relaciona con HTML y CSS |
- Aprender a jugar con JavaScript en nuestros exploradores |
- Y algunas estrategias para: |
- Elegir una librería de JavaScript |
- Comprender rápidamente ejemplos de código |
- Comenzar a (romper) modificar código JavaScript |

---

## Entonces... ¿Qué es JavaScript?

- Lenguaje de programación creado en 1995 para ser incluido en el navegador web de Netscape.
- Originalmente desarrollado para permitir la programación en los exploradores web y desarrollar páginas dinámicas.
- Hoy es ampliamente utilizado también del lado del servidor.

---?image=assets/images/js_popularity_on_github.png&size=60% 60%
@title[Popularidad en GitHub]

@snap[north-east]
<h4>Los lenguajes de programación más populares según <a href="https://octoverse.github.com/">GitHub</a></h4>
@snapend

---?image=assets/images/preguntas_en_stack_overflow.png&size=55% 55%
@title[Popularidad en StackOverflow]

@snap[north-east]
<h4>Los lenguajes de programación más "preguntados" según <a href="https://insights.stackoverflow.com/trends?tags=java%2Cpython%2Cc%23%2Cjavascript%2Cphp%2Cruby">StackOverflow</a></h4>
@snapend

---

### ¿Por qué es tan importante JavaScript?

- Funciona en todos los exploradores web.
- Es uno de los lenguajes de programación más populares y activos del mundo.
- Junto con HTML y CSS, JavasScript es una de las tres principales tecnologías de Internet.

---

#### [Thimble](http://thimbleprojects.org): una herramienta para aprender HTML, CSS y JavaScript

- Es un editor de código en línea
- Creado para editar y publicar páginas web
- Y al mismo tiempo aprender HTML, CSS y JavaScript
- Desarrollado por [Mozilla](https://www.mozilla.org), los creadores de [Firefox](https://www.mozilla.org/en-US/firefox/)

---

### Una alternativa para usuarios avanzados: [StackBlitz](https://stackblitz.com/)

- Es un IDE completo online para desarrollar proyectos enteros con Js
- Permite crear proyectos de Angular, React y Vue, entre otros.
- Construido con la misma tecnología que Visual Studio Code
- Permite importar paquetes npm
- [Más información](https://medium.com/@ericsimons/stackblitz-online-vs-code-ide-for-angular-react-7d09348497f4)

---

### HTML - Hyper Text Markup Languaje

- Define la *estructura* de una paǵina web
- Su versión inicial data de 1991
- Mediante etiquetas (tags) define la estructura del documento, sus vínculos con otros documentos (hipertexto) y la presentación de algunos elementos.

---

### Un ejemplo de HTML

[Ver](https://thimbleprojects.org/opensas/533202) | [Remix](https://thimble.mozilla.org/es/projects/533202/remix)

```html
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hecho con Mozilla Thimble para la Media Party 2018</title>
  </head>
  <body>
    <h1>¿Quién le teme a <i>JavaScript</i>?</h1>
    <p>
      Periodista temeroso: <strong>desconocido</strong>
    </p>
  </body>
</html>
```
---

### CSS - Cascading Style Sheet

- Define la *presentación* de una paǵina web
- Es un lenguaje de hojas de estilo que describe la manera en que un documento escrito en html será presentado.
- Utiliza selectores para especificar a qué elementos aplicar los estilos
- La primera versión oficial es de 1996
- Para ver qué es posible hacer con CSS visiten [www.csszengarden.com/](http://www.csszengarden.com/)

---

### Un ejemplo de CSS

[Ver](https://thimbleprojects.org/opensas/533205) | [Remix](https://thimble.mozilla.org/es/projects/533205/remix)

```html
<style>
  p {
    font-family: 'helvetica neue', helvetica, sans-serif;
    letter-spacing: 1px;
    text-transform: uppercase;
    text-align: center;
    border: 2px solid rgba(0,0,200,0.6);
    background: rgba(0,0,200,0.3);
    color: rgba(0,0,200,0.6);
    box-shadow: 1px 1px 2px rgba(0,0,200,0.4);
    border-radius: 61px;
    padding: 3px 10px;
    display: inline-block;
    cursor:pointer;
  }
</style>
```

---

### JavaScript

- Define el *comportamiento* de una paǵina web
- Es un lenguaje de programación que permite crear páginas web dinámicas.
- Mediante el mismo podemos acceder a cada elemento de la página web y modificarlo programáticamente.

Note:

- HTML y CSS definen el contenido inicial de la página, lo que el explorador recibe desde el servidor
- A partir de ahí el contenido se modifica mediante js
- o pidiendo otra página web
- Analogía con un libro: js reescribe la página actual

---

### Un ejemplo de JavaScript

[Ver](https://thimbleprojects.org/opensas/533207) | [Remix](https://thimble.mozilla.org/es/projects/533207/remix)

```javascript
<script>
  var parrafo = document.querySelector('p');

  parrafo.addEventListener('click', actualizarNombre);

  function actualizarNombre() {
    var nombre = prompt('¿Cómo te llamás?');
    if (nombre === '') nombre = 'desconocido';
    parrafo.innerHTML = 'Periodista temeroso: <strong>' + nombre + '</strong>';
  }
</script>
```

---

### Poniendo a prueba todo esto con Thimble

- Crear una cuenta en [Thimble](https://thimble.mozilla.org)
- Hagan un "remix" de este [proyecto](https://thimbleprojects.org/opensas/533202/) (o abrilo en [StackBlitz](https://stackblitz.com/edit/js-que-es-javascript-01))
- Hagan click en "tutorial" y sigan las instrucciones
- Cuando terminen hagan click en el botón "Publicar"
- Visitar la nueva página creada

Note:

- En cada paso vayan identificando qué es HTML, CSS y JavaScript y qué rol cumple en la construcción de la página web
- Parte del ejercicio es intentar comprender qué hace cada cosa aunque no tengan muy en claro qué está pasando.
- Mostrar el código corriendo en el explorador
- Mostrar el código fuente
- Mostrar el depurador

---

### JavaScript 101: Tipos de Datos

En el mundo de las computadoras todo es un dato, que es en esencia una secuencia de unos y ceros.

En JavaScript, cada dato es un "valor", el cual tiene un determinado "tipo" que especifica qué podemos hacer con él.

Hay dos tipos de datos: primitivos y objetos (o complejos)

---

### JavaScript 101: Datos Primitivos

- _strings_: Cadenas de texto

- _numbers_: números

- _booleans_: valores lógicos,  verdadero o falso (true / false)

- _null_: valores vacíos

- _undefined_: valores que no han sido definidos aún

---

### Tipo de Datos: Jugando con la consola

- En chrome o Firefox presionamos F12 o botón derecho _Inspect_

- Seleccionamos la opción "Consola"

Note:

- la consola del depurador brinda autocomplete

---

#### Jugando con la consola: Strings

```javascript
>> 'cadena de texto'
<- "cadena de texto"

>> 'cadena de texto'.length
<- 15

>> typeof('cadena de texto')
<- "string"

>> 'cadena de texto' + 'otra cadena'
<- "cadena de textootra cadena"

>> 'abeja' > 'abaco'
<- true
```

---

#### Jugando con la consola: Strings

```javascript
>> 'cadena de texto'.substring(5,11)
<- "a de t"

>> 'cadena de texto'.split(' ')
<- Array(3) [ "cadena", "de", "texto" ]

>> 'cadena de texto'.startsWith('cadena')
<- true

>> 'cadena de texto'.startsWith('texto')
<- false

>> 'cadena de texto'.endsWith('texto')
<- true

>> 'cadena de texto'.toUpperCase()
<- "CADENA DE TEXTO"
```

---

#### Jugando con la consola: Numbers

```javascript
>> 22
<- 22

>> typeof(22)
<- "number"

>> (22).toString
<- ƒ toString() { [native code] }

>> (22).toString()
<- "22"

>> 22 < 22.00001
<- true
```

---

#### Jugando con la consola: Booleans

```javascript
>> // operadores logicos: NOT -> !, AND -> &&, OR -> ||

>> 1 === 2
<- false

>> !(1 === 2)
<- true

>> !false
<- true

>> 10 > 5 && 10 > 20
<- false

>> 10 > 5 || 10 > 20
<- true
```

---

### JavaScript 101: Variables

- Las variables son contenedores que almacenan un valor.

- Deben tener un nombre que debe ser único.

- Tenemos que declaralas con la sentencia 'let', 'var' o 'const'.

- Les asignamos valores con el operador "=".

- Podemos asignarles otros valores

---

#### Jugando con la consola: Variables

```javascript
>> let nombre
<- undefined

>> typeof(nombre)
<- "undefined"

>> nombre = 'Sebastian'
<- "Sebastian"

>> typeof(nombre)
<- "string"

>> nombre = 22
<- 22

>> typeof(nombre)
<- "number"
```

Note:

Las variables funcionan en remplazo del valor que contiene

---

#### JavaScript 101: Datos Complejos (Objetos)

- Arrays: tienen un conjunto de elementos a los cuales se accede mediante un índice númerico.

- Objetos: están compuestos por un conjunto de propiedades, cada una con un nombre y valor. Se accede al valor de cada propiedad mediante el nombre.

- Funciones: son programas que pueden ser ejecutados. Pueden recibir valores como parámetros y retornar un valor

---

#### Jugando con la consola: Arrays

```javascript
>> let miArray = [1, 'hola', true, null, 20]
<- undefined

>> miArray[0]
<- 1

>> miArray[1]
<- "hola"

>> miArray[2]
<- true

>> miArray[3]
<- null

>> miArray[4]
<- 20

>> miArray[5]
<- undefined

>> miArray.length
<- 5
```

---

#### Jugando con la consola: Objetos

```javascript
>> let evento = {
    nombre: 'Media Party',
    anio: 2018,
    espectacular: true,
    fecha: '23-08-2018',
    disertantes: [ 'jeff jarvis', 'amanda hickman', 'farida aletta vis', 'aron piholfer', 'et al...']
}
<- undefined

>> evento.nombre
<- "Media Party"

>> evento.web                               // propiedad no definida
<- undefined

>> evento.web = 'http://mediaparty.info/'   // agrego una nueva propiedad
<- "http://mediaparty.info/"

>> evento.web
<- "http://mediaparty.info/"
```

---

#### Jugando con la consola: Funciones

```javascript
>> function sorprendente(text) {
    return '¡' + text + '!';
}
<- undefined

>> sorprendente('Media Party 2018')
<- "¡Media Party 2018!"

>> function concatena(string1, string2) {
    return string1 + string2;
}

>> concatena('hola', 'mundo')
<- "holamundo"

>>> function primeraEnMayuscula(texto) {
    return texto.substr(0,1).toUpperCase() + texto.substr(1);
}
<- undefined

>>> primeraEnMayuscula('hola')
<- "Hola"
```

---

#### y por supuesto, todo esto se puede combinar y anidar

```javascript
let exponente = {
    nombre: 'sebastian scarano',
    alias: 'opensas',
    workshop: {
        nombre: 'Como aprendí a perderle el miedo a JavaScript',
        asistentes: [
            { nombre: 'Felipe Juárez', experiencia: 'principiante' },
            { nombre: 'Mariela Gutierrez', experiencia: 'avanzado' }
        ]
    },
    saludame: function() {
      return 'hola desde el usuario opensas';
    }
}

>>> usuario.workshop.asistentes[1].nombre
<- "Mariela Gutierrez"

>>> usuario.saludame
<- ƒ () {
      return 'hola desde el usuario opensas';
    }

>>> usuario.saludame()
<- 'hola desde el usuario opensas'
```
---

#### JSON: JavaScript Object Notation

- Es un formato de intercambio de información

- Está basado en la definición de objetos de JavaScript

- Es utilizado por multiples APIs

---

#### Ejemplo de JSON: google maps

Haciendo click en [link](http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=Sarmiento%203131,%20caba,%20argentina) estaremos consultando la información de google maps para la dirección "Sarmiento 3131, caba, argentina"

```javascript
http://maps.googleapis.com/maps/api/geocode/json?sensor=false&address=Sarmiento%203131,%20caba,%20argentina

{
  "results":[
    {
      "address_components":[
        {
          "long_name":"3131",
          "short_name":"3131",
          "types":[
            "street_number"
          ]
        },
        {
          "long_name":"Sarmiento",
          "short_name":"Sarmiento",
          "types":[
            "route"
          ]
        },
        {
          "long_name":"Balvanera",
          "short_name":"Balvanera",
          "types":[
            "political",
            "sublocality",
            "sublocality_level_1"
          ]
        },
        {
          "long_name":"Comuna 3",
          "short_name":"Comuna 3",
          "types":[
            "administrative_area_level_2",
            "political"
          ]
        },
        {
          "long_name":"Buenos Aires",
          "short_name":"CABA",
          "types":[
            "administrative_area_level_1",
            "political"
          ]
        },
        {
          "long_name":"Argentina",
          "short_name":"AR",
          "types":[
            "country",
            "political"
          ]
        },
        {
          "long_name":"C1196",
          "short_name":"C1196",
          "types":[
            "postal_code"
          ]
        }
      ],
      "formatted_address":"Sarmiento 3131, C1196 CABA, Argentina",
      "geometry":{
        "bounds":{
          "northeast":{
            "lat":-34.6061716,
            "lng":-58.4102729
          },
          "southwest":{
            "lat":-34.6066223,
            "lng":-58.4106727
          }
        },
        "location":{
          "lat":-34.6064221,
          "lng":-58.4104254
        },
        "location_type":"ROOFTOP",
        "viewport":{
          "northeast":{
            "lat":-34.6050479697085,
            "lng":-58.40912381970849
          },
          "southwest":{
            "lat":-34.6077459302915,
            "lng":-58.4118217802915
          }
        }
      },
      "place_id":"ChIJpeIfRvPKvJUR62r4USkMbPA",
      "types":[
        "premise"
      ]
    }
  ],
  "status":"OK"
}
```

---

#### Otro ejemplo de JSON: los resultados del mundial

En [este archivo](https://github.com/openfootball/world-cup.json/blob/master/2018/worldcup.json) están los resultados del último mundial en formato json.

En este [link](https://thimbleprojects.org/opensas/533507/) hay una proyecto hecho con thimble que utiliza este archivo.

Lean atentamente los comentarios que les dan varias pistas acerca de cómo resolverlo

Hacer un remix de este proyecto y completar el código para traer el resultado del primer partido de argentina.

[Aquí](https://thimbleprojects.org/opensas/533622/) está la solución del ejercicio.

Note:

- Mostrar el funcionamiento de depurador de Chrome

- Breakpoints, inspeccionar variables, etc.

---

### Customizando un perfil de usuario

Un ejemplo más realista

Una página que muestra un perfil de usuario, utilizando varios frameworks.

En este [link](https://thimbleprojects.org/opensas/533499/) está el thimble para que lo miren y adapten con sus datos.

Note:

- Mostrar cómo analizar el código

- y detectar que cosas hay que cambiar y cuales no

- archivos incluidos, frameworks, idioma de las variables, etc.

- mostrar que la información es dinámica

---

### JavaScript 101: Bucles

```javascript
>> let palabras = ['bienvenidos', 'a', 'la', 'Media', 'Party'];
<- undefined

>> let frase = ''
<- undefined

>> for (let i = 0; i < palabras.length; i++) {
    console.log(i, palabras[i]);
    frase = frase + palabras[i] + ' ';
}

<- 0 "bienvenidos"
<- 1 "a"
<- 2 "la"
<- 3 "Media"
<- 4 "Party"

>> frase
<- "bienvenidos a la Media Party "
```

---

### JavaScript 101: Bucles otra manera

```javascript
>> let palabras = ['bienvenidos', 'a', 'la', 'Media', 'Party'];
<- undefined

>> let frase = ''
<- undefined

>> palabras.forEach( function(palabra) {
    frase = frase + palabra + ' ';
})
<- undefined

>> frase
<- "bienvenidos a la Media Party "
```

---

### JavaScript 101: Ejecución condicional

```javascript
>>> function esMayorDeEdad(edad) {
    if (edad >= 21) {
        return true;
    } else {
        return false;
    }
}
<- undefined

>>> esMayorDeEdad(20.9)
<- false

>>> esMayorDeEdad(21)
<- true

>>> function esMayorDeEdad(edad) { return (edad >= 21); }  // mas corto
<- undefined
```

---

### JavaScript 101: Arreglamos un bug

El primer ejemplo tiene un "bug".

Prueben ejecutarlo a través de este [link](https://thimbleprojects.org/opensas/533207/), hagan click en "desconocido" y aprieten ESC.

Intenten comprender que pasa y solucionarlo

Tip: usar el depurador

Note:

- mostrar que se pueden modificar los elementos
>>> parrafo.style = 'background-color:red'

- explicar callbacks

---

### Finalmente: Leaflet js

- Es una de las librerías js más utilizadas para desplegar mapas

- Vamos a seguir la [guía](https://leafletjs.com/examples/quick-start/) para comenzar a utilizarla

- y ver si ahora entendemos algo!!!

---

#### Ejemplo Leaflet 01 - Primeros pasos

- Ver [thimble](https://thimbleprojects.org/opensas/533714/)

- Bajamos el [ejemplo inicial](https://leafletjs.com/examples/quick-start/) de [Leaflet](https://leafletjs.com/)

- Lo centramos en el Konex

- Agregamos un callback para mostrar un popup con las coordenadas del punto seleccionado

---

#### Ejemplo Leaflet 02 - Agregamos puntos

- Ver [thimble](https://thimbleprojects.org/opensas/533715/)

- Agregamos 3 puntos al mapa

---

#### Ejemplo Leaflet 03 - Trabajando con arrays

- Ver [thimble](https://thimbleprojects.org/opensas/533717/)

- Creamos un array con los puntos

- Agregamos los puntos al mapa con un bucle que recorre el array

- Agregamos algunos puntos más

---

#### Ejemplo Leaflet 04 - Nuestra propia librería

- Ver [thimble](https://thimbleprojects.org/opensas/533721/)

- creamos la funcion _inicializarMapa_ con toda la funcionalidad general

- creamos el archivo _mapa.js_ con la funcion inicializarMapa

---

#### Ejemplo Leaflet 05 - [Google docs](https://docs.google.com/spreadsheets/)

- Crear una nueva planilla en [Google docs](https://docs.google.com/spreadsheets/) con los puntos del mapa

- _Archivo_ -> _Publicar en la web_

- _Compartir_ -> _Configuración avanzada_ -> _Cualquier usuario con el vínculo_ - _puede ver_

- Copiar el _vínculo para compartir_

- Atención: ver [estas notas](https://gist.github.com/jsvine/3295633) antes de usar en producción

Note:

- Mostrar la [planilla](https://docs.google.com/spreadsheets/d/1rK4M87X5jedKQhSDIvbtQYBDr0DLRpzfVatKhAEdgFQ/edit?ouid=117706226112587982416&usp=sheets_home&ths=true) de google docs

---

#### Ejemplo Leaflet 05 - Nuestro propio backend con [Google docs](https://docs.google.com/spreadsheets/)

- Ver [thimble](https://thimbleprojects.org/opensas/533846/)

- agregamos la librería [tabletop](https://github.com/jsoma/tabletop)

- creamos la funcion gdocsCargarPlanilla siguiendo [la documentación](https://github.com/jsoma/tabletop#2-setting-up-tabletop) de tabletop

- cargamos dinámicamente los puntos a partir de nuestra planilla en google docs

Note:

- Mostrar ejemplo de llamada json

---

#### Ejemplo Leaflet 06 - [Airtable](https://airtable.com)

- Es una base de datos personal en línea, una mezcla de Excel y Trello

- Creamos una [planilla](https://airtable.com/invite/l?inviteId=invHjt9OEwG5TUSSc&inviteToken=fdca6e57f14c768efa309bab1219d11397510a16976cd808307fb943b86d18d7) con los puntos del mapa

- Generamos una _key_ para utilizar la [api](https://airtable.com/account)

- Consultamos la [documentación](https://airtable.com/api) de la api

- Usamos nuestra [API](https://api.airtable.com/v0/appSLzp1WuL9ik8fJ/Table%201?api_key=keyx7FrPMoI1SDzTj) desde JavaScript para traer los puntos

- Más info: [comparativa](https://stackshare.io/stackups/airtable-vs-sheetsee-js-vs-tabletop-js) entre [Airtable](https://airtable.com), [tabletop](https://github.com/jsoma/tabletop) y [Sheetsee.js](http://jlord.us/sheetsee.js)
---

#### Ejemplo Leaflet 06 - Nuestro propio backend con [Airtable](https://airtable.com)

- Ver [thimble](https://thimbleprojects.org/opensas/533921)

- creamos una planilla en [airtable](https://airtable.com/) con los puntos del mapa

- creamos una librería para leer la información de airtable desde JavaScript

- cargamos dinámicamente los puntos a partir de nuestra planilla en airtable

---

#### Otro ejemplo: [C3.js](https://c3js.org/)

- Librería de gráficos basada en D3.js

- Tomamos el [ejemplo inicial](https://c3js.org/gettingstarted.html)

- Ver [thimble](https://thimbleprojects.org/opensas/534012)

---

#### Ejemplo C3 paso 2 - exploramos opciones

- Ver [thimble](https://thimbleprojects.org/opensas/533110)

- Exploramos todos [los ejemplos](https://c3js.org/examples.html) y tomamos las opciones que nos pueden ser útiles

---

#### Ejemplo C3 paso 3 - Nuestra propia librería

- Ver [thimble](https://thimbleprojects.org/opensas/534027)

- Cargamos la configuración de los datos en un array

- Creamos una librería para crear el gráfico a partir del array

---

#### Ejemplo C3 paso 4 - Backend con [Google docs](https://docs.google.com/spreadsheets/)

- Ver [thimble](#TODO)

- creamos una planilla en [Google docs](https://docs.google.com/spreadsheets/) y la compartimos

- agregamos la librería [tabletop](https://github.com/jsoma/tabletop)

- reutilizamos la funcion gdocsCargarPlanilla siguiendo [la documentación](https://github.com/jsoma/tabletop#2-setting-up-tabletop) de tabletop

- cargamos dinámicamente los valores a partir de nuestra planilla en google docs

---

#### Ejemplo C3 paso 5 - Backend con [Airtable](https://airtable.com)

- Ver [thimble](https://thimbleprojects.org/opensas/534086/)

- creamos una planilla en [AirTable](https://airtable.com/)

- reutilizamos la libreria _airtablePlanilla_ para leer la información de AirTable desde JavaScript

- generamos nuestra [apikey](https://airtable.com/account)

- consultamos la [documentacion](https://airtable.com/api) para ver conectarnos a nuestra planilla

- cargamos dinámicamente la información del gráfico a partir de nuestra planilla en airtable

---


#### Un ejemplo completo con JavaScript, Leaflet y la API de [Carto](https://carto.com/) (ex CartoDB)

- Mapa cultura: mapa interactivo con la información de entidades culturales de todo el país

- Presentado en la Media Party 2014, mostrando el uso de [ckan](https://ckan.org), [OpenRefine](http://openrefine.org), [Carto](https://carto.com/), Sql, [GitHub](https://github.com/), JavaScript y [LeafLet](https://leafletjs.com).

---

#### Un ejemplo completo con JavaScript, Leaflet y la API de [Carto](https://carto.com/) (ex CartoDB)

- El proyecto completo en [Thimble](https://thimbleprojects.org/opensas/534091)

- La aplicación corriendo en [www.nardoz.com/mapa-cultura](http://www.nardoz.com/mapa-cultura)

- Aquí está [la presentación](http://www.nardoz.com/mapa-cultura/slides/mediaparty_rendered.svg) y el [repositorio de GitHub](https://github.com/Nardoz/mapa-cultura)

Note:

mostar un ejemplo del consumo de la api:

- Ejemplo de consumo de la API de CartoDB: <a href="https://devel.cartodb.com/api/v2/sql?q=select%20tipo%2C%20subtipo%2C%20nombre%2C%20direccion%2C%20telefono%2C%20email%2C%20web%2C%20lat%2C%20lon%20from%20cultura%20where%20(ST_Within(the_geom%2C%20ST_Envelope(ST_GeomFromText(%27LINESTRING(%20-58.58865516919517%20-34.46638983527999%2C%20-58.26502067308998%20-34.78320404698995%20)%27%2C%204326))))%20and%20(lower(nombre)%20like%20%27%25corrientes%25%27%20or%20lower(direccion)%20like%20%27%25corrientes%25%27)%20and%20((lower(subtipo)%20in%20(%27salas%20de%20cine%27%2C%20%27espacios%20incaa%27)))">cines de la calle corrientes</a>

---?image=assets/images/gitpitch-audience.jpg

### Muchas gracias a todos!

## ¿Preguntas?

<br>

@fa[twitter gp-contact]&nbsp;[@opensas](https://twitter.com/opensas)

@fa[github gp-contact]&nbsp;[opensas](https://github.com/opensas)

@fa[gitlab gp-contact]&nbsp;[opensas](https://gitlab.com/opensas/js_para_periodistas)

---

## Agradecimientos

<br>
@fa[github gp-contact]&nbsp;[leandrogatti](https://github.com/leandrogatti) - Leandro Gatti

@fa[twitter gp-contact]&nbsp;[@gvilarino](https://twitter.com/gvilarino) - Guido Vilariño

@fa[twitter gp-contact]&nbsp;[@impronunciable](https://twitter.com/impronunciable) - Dan Zajdband

@fa[twitter gp-contact]&nbsp;[@deimidis](https://twitter.com/deimidis) - Guillermo Movia

@fa[twitter gp-contact]&nbsp;[@mozilla](https://twitter.com/mozilla) - Mozilla