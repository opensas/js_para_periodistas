https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab

twittear: contar donde esta el repo y la presentacion

- presentacion

- repo: invitar a bajar el codigo

- consultar experiencia de la audiencia
- reunir por experiencia

---

objetivo

agenda


ejercicio 1 - que es js
--

preguntar que conclusión sacaron


--

difusion:

Más de una docena de Thimbles esperando a que te animes a programar en la web. https://gitlab.com/opensas/js_para_periodistas#thimbles-de-ejemplo (Gran herramienta!!! felicitaciones @mozilla) Hoy a las 13.30 en el Konex, venía a la #MediaParty y perdele el miedo a JavaScript.
Anótense acá: https://mediaparty2018.sched.com/event/FzbK, los espero!

--

En un par de horitas arrancamos con el taller de intro a la programación web en la #MediaParty : Cómo aprendí a perderle el miedo a JavaScript.

Acá está el repositorio en github: https://gitlab.com/opensas/js_para_periodistas#js-para-periodistas

Y la presesentación online: https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/
@hackshackersba


AHORA arrancamos con el taller de intro a la programación web en la #MediaParty : Cómo aprendí a perderle el miedo a JavaScript.

Acá está el repositorio en github: https://gitlab.com/opensas/js_para_periodistas#js-para-periodistas

Y la presesentación online: https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/
@hackshackersba
--

En apenas unas horitas arrancamos con el taller de programación web, para que le pierden el miedo de una vez por todas a JavaScript.

Acá está el repositorio con todos los ejemplos: https://gitlab.com/opensas/js_para_periodistas#js-para-periodistas

Si tienen ganas de correr los ejemplos en sus máquinas pueden descargar el archivo comprimido: https://gitlab.com/opensas/js_para_periodistas/-/archive/master/js_para_periodistas-master.zip

O clonar el proyecto entero con "git clone https://gitlab.com/opensas/js_para_periodistas.git"

Acá está la presentación para que la vayan mirando: https://gitpitch.com/opensas/js_para_periodistas?grs=gitlab#/

Y los "thimbles" con los ejemplos de código que vamos a ver: https://gitlab.com/opensas/js_para_periodistas#thimbles-de-ejemplo

Nos vemos pronto

Saludos

Sas
