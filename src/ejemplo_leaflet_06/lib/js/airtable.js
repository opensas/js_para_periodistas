function airtablePlanilla(planillaId, apiKey, callback) {

  const log = false;

  //https://api.airtable.com/v0/appSLzp1WuL9ik8fJ/Table%201?api_key=keyx7FrPMoI1SDzTj
  const url = 'https://api.airtable.com/v0/' + planillaId + '?api_key=' + apiKey;

  if (log) console.log('about to fetch', url);
  jsonFetch(url, function (json) {
    let registros = extraerCampos(json);
    if (log) console.log(JSON.stringify(registros));
    callback(registros);
  });

  function extraerCampos(table) {
    let registros = [];
    table.records.forEach(function(record) {
      registros.push(record.fields);
    });
    return registros;
  }

};
