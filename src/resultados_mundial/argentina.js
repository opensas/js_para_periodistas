let argentina = [
  {
    "num": 7,
    "date": "2018-06-16",
    "time": "16:00",
    "team1": {
      "name": "Argentina"
    },
    "team2": {
      "name": "Iceland"
    },
    "score1": 1,
    "score2": 1,
    "score1i": 1,
    "score2i": 1,
    "goals1": [
      {
        "name": "Agüero",
        "minute": 19
      }
    ],
    "goals2": [
      {
        "name": "Finnbogason",
        "minute": 23
      }
    ]
  },
  {
    "num": 23,
    "date": "2018-06-21",
    "time": "21:00",
    "team1": {
      "name": "Argentina"
    },
    "team2": {
      "name": "Croatia"
    },
    "score1": 0,
    "score2": 3,
    "score1i": 0,
    "score2i": 0,
    "goals1": [
    ],
    "goals2": [
      {
        "name": "Rebić",
        "minute": 53
      },
      {
        "name": "Modrić",
        "minute": 80
      },
      {
        "name": "Rakitić",
        "minute": 90,
        "offset": 1
      }
    ]
  },
  {
    "num": 39,
    "date": "2018-06-26",
    "time": "21:00",
    "team1": {
      "name": "Nigeria"
    },
    "team2": {
      "name": "Argentina"
    },
    "score1": 1,
    "score2": 2,
    "score1i": 0,
    "score2i": 1,
    "goals1": [
      {
        "name": "Moses",
        "minute": 51,
        "penalty": true
      }
    ],
    "goals2": [
      {
        "name": "Messi",
        "minute": 14
      },
      {
        "name": "Marcos Rojo",
        "minute": 86
      }
    ]
  },
  {
    "num": 50,
    "date": "2018-06-30",
    "time": "17:00",
    "team1": {
      "name": "France"
    },
    "team2": {
      "name": "Argentina"
    },
    "score1": 4,
    "score2": 3,
    "score1i": 1,
    "score2i": 1,
    "score1et": null,
    "score2et": null,
    "score1p": null,
    "score2p": null,
    "knockout": true,
    "goals1": [
      {
        "name": "Antoine Griezmann",
        "minute": 13,
        "penalty": true
      },
      {
        "name": "Benjamin Pavard",
        "minute": 57
      },
      {
        "name": "Kylian Mbappé",
        "minute": 64
      },
      {
        "name": "Kylian Mbappé",
        "minute": 68
      }
    ],
    "goals2": [
      {
        "name": "Ángel Di María",
        "minute": 41
      },
      {
        "name": "Gabriel Mercado",
        "minute": 48
      },
      {
        "name": "Sergio Agüero",
        "minute": 90,
        "offset": 3
      }
    ]
  }
];