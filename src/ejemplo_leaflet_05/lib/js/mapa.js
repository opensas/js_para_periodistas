function inicializarMapa(htmlid, centro, puntos, zoom) {

  // valores por defecto
  if (centro === undefined) centro = [-34.606318, -58.410753];
  if (zoom === undefined) zoom = 13;
  if (puntos === undefined) puntos = [];

  let mymap = L.map(htmlid).setView(centro, zoom);

  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFwYm94IiwiYSI6ImNpejY4NXVycTA2emYycXBndHRqcmZ3N3gifQ.rJcFIG214AriISLbB6B5aw', {
    maxZoom: 18,
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, ' +
      '<a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, ' +
      'Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    id: 'mapbox.streets'
  }).addTo(mymap);

  let popup = L.popup();

  function onMapClick(e) {
    console.log(e.latlng);
    popup
      .setLatLng(e.latlng)
      .setContent("Hiciste click en " + e.latlng.toString())
      .openOn(mymap);
  }

  mymap.on('click', onMapClick);

  puntos.forEach(function (punto) {
    let marker = L.marker([punto.lat, punto.lon]).addTo(mymap);
    marker.bindPopup(punto.nombre);
  });

  return mymap;
};