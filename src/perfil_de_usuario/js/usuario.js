let usuario = {
  sinAvatar: 'images/avatar0.jpg',
  nombre: 'Sebastián Scarano',
  avatar: 'https://i.pinimg.com/originals/17/f9/9e/17f99e88dbdf88fe354c8f2f2f4eea80.jpg',
  alias: 'opensas',
  ubicacion: 'Buenos Aires, Argentina',
  edad: 42,
  web: 'opensas.wordpress.com',
  facebook: 'sebastian.scarano.3',
  twitter: 'opensas',
  bio: 'Defensor del software libre y demás libertades',
  amigos: [
    {
      nombre: 'Juan',
      avatar: 'images/avatar1.jpg'
    },
    {
      nombre: 'Ernesto',
      avatar: 'images/avatar2.jpg'
    },
    {
      nombre: 'Mariana',
      avatar: 'images/avatar3.jpg'
    },
    {
      nombre: 'Julián',
      avatar: 'images/avatar4.jpg'
    },
    {
      nombre: 'Lorena',
      avatar: 'images/avatar5.jpg'
    },
    {
      nombre: 'Leandro',
      avatar: 'images/avatar6.jpg'
    }
  ],
  fotos: [
    'images/image1.jpg',
    'images/image2.jpg',
    'images/image3.jpg',
    'images/image4.jpg',
    'images/image5.jpg',
    'images/image6.jpg',
  ],
  enviarMensaje: function() {
    alert('Mensaje enviado');
  }
};

var app = new Vue({
  el: '#app',
  data: usuario,
  computed: {
    facebookUrl: function() {
      return this.facebook ? 'https://www.facebook.com/' + this.facebook : '';
    },
    twitterUrl: function() {
      return this.twitter ? 'https://twitter.com/' + this.twitter : '';
    }
  }
});