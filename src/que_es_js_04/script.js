var parrafo = document.querySelector('p');

parrafo.addEventListener('click', actualizarNombre);

function actualizarNombre() {
  var nombre = prompt('¿Cómo te llamás?');
  if (nombre === '') nombre = 'desconocido';
  parrafo.innerHTML = 'Periodista temeroso: <strong>' + nombre + '</strong>';
}