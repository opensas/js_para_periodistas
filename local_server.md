## Corriendo los ejemplos en un servidor local

Para poder correr los ejemplos tenés que ejecutar un servidor local y navegar a la paǵina _index.html_. Allí encontrarás links para llegar a los ejemplos que están en la carpeta src.

También podés iniciar el servidor local desde la carpeta de cada ejemplo.

A cotinuación, varias opciones para instalar y correr un servidor local.

### Servidor local con node.js

Con [node.js](https://nodejs.org/es/download/) instalado:

```bash
$ npm install -g http-serve
+ http-serve@1.0.1
updated 2 packages in 4.72s

$ http-serve
Starting up http-serve for ./
Available on:
  http://127.0.0.1:8080
  http://192.168.0.12:8080
Hit CTRL-C to stop the server
```
### Servidor local con Python

Con [python](https://www.python.org/downloads/) instalado:

```bash
# Para python 3.X o superior
$ python -m http.server
Serving HTTP on 0.0.0.0 port 8000 (http://0.0.0.0:8000/) ...

# Para python 2.X
python -m SimpleHTTPServer
serving at port 8000
```
### Servidor local con Caddy

Si preferís instalar un servidor local sin ninguna dependencia externa, podés instalar [Caddy](https://caddyserver.com/download)

Creá un archivo CaddyFile con el siguiente contenido:

```
localhost:8080
```

Y ejecutá el siguiente comando:

```
$ caddy -conf CaddyFile
Activating privacy features... done.
http://localhost:8080

```
